<?php

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="book")
 */
class Book
{

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\SequenceGenerator(sequenceName="id", initialValue=1)
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=250, unique=true, nullable=false)
     */
    private $name;

    /**
     * @var Collection|Author[]
     *
     *@ORM\ManyToMany(targetEntity=Author::class)
     */
    private $author;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return Author[]|Collection
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param Author[]|Collection $author
     */
    public function setAuthor($author): void
    {
        $this->author = $author;
    }

}