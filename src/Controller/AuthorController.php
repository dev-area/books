<?php


namespace App\Controller;


use App\Entity\Author;
use App\Form\Type\AuthorType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class AuthorController extends AbstractApiController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function createAction(Request $request): Response
    {
        $form = $this->buildForm(AuthorType::class);
        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->respond($form, Response::HTTP_BAD_REQUEST);
        }

        /** @var Author $author */
        $author = $form->getData();

        $currentAuthor = $this->entityManager->getRepository(Author::class)->findOneBy([
            'name' => $author->getName()
        ]);

        if(!empty($currentAuthor)){
            throw new NotFoundHttpException('This Author already exists');
        }

        $this->entityManager->persist($author);
        $this->entityManager->flush();

        return $this->respond($author);
    }

}