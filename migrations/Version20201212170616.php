<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201212170616 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
//        $authors = ['Пушкин', 'Лермонтов', 'Толстой', 'Чехов', 'Достоевский', 'Гоголь', 'Булгаков'];
//        for ($i = 1; $i <= 10000; $i++) {
//            $author = $authors[array_rand($authors, 1)] . "_" . uniqid();
//            $sql = 'INSERT INTO "public"."author" ("id", "name") VALUES (' . $i . ', \'' . $author . '\')';
//            $this->addSql($sql);
//        }
//
//        $books = ['Eugene Onegin|Евгений Онегин'
//            , 'A Hero of Our Time|Герой нашего времени'
//            , 'War And Peace|Война и мир'
//            , 'The Cherry Orchard|Вишневый Сад'
//            , 'Crime and Punishment|Преступление и наказание'
//            , 'Dead souls|Мёртвые души'
//            , 'The Master and Margarita|Мастер и Маргарита'
//        ];
//        for ($i = 1; $i <= 10000; $i++) {
//            $book = $books[array_rand($books, 1)] . "|" . uniqid();
//            $sql = 'INSERT INTO "public"."book" ("id", "name") VALUES (' . $i . ', \'' . $book . '\')';
//            $this->addSql($sql);
//        }
//
//        $limit = 10000;
//        for ($i = 1; $i <= $limit; $i++) {
//            $n = rand(1, 2);
//            $t = $limit / $n;
//            $min = 1;
//            for ($j = 1; $j <= $n; $j++) {
//                $a = rand($min, $j * $t);
//                $sql = 'INSERT INTO "public"."book_author" ("book_id", "author_id") VALUES (' . $i . ', ' . $a . ')';
//                $this->addSql($sql);
//                $min += $t;
//            }
//
//        }

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
