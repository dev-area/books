<?php


namespace App\Controller;


use App\Entity\Book;
use App\Form\Type\BookType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class BookController extends AbstractApiController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function createAction(Request $request): Response
    {
        $form = $this->buildForm(BookType::class);
        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->respond($form, Response::HTTP_BAD_REQUEST);
        }

        /** @var Book $book */
        $book = $form->getData();

        $currentBook = $this->entityManager->getRepository(Book::class)->findOneBy([
            'name' => $book->getName()
        ]);

        if (!empty($currentBook)) {
            throw new NotFoundHttpException('This Book already exists');
        }

        $this->entityManager->persist($book);
        $this->entityManager->flush();

        return $this->respond($book);
    }

    public function showAction(Request $request): Response
    {
        $bookId = $request->get('id');

        if (!$bookId) {
            throw new NotFoundHttpException('Book not found');
        }

        $book = $this->entityManager->getRepository(Book::class)->createQueryBuilder('b')
            ->select('b.id, b.name')
            ->where('b.id = :id')
            ->setParameter('id', $bookId)
            ->getQuery()
            ->getSingleResult();

        if (empty($book)) {
            throw new NotFoundHttpException('Book does not exist');
        }

        $translations = explode("|", $book['name']);

        if ($request->getLocale() == 'ru') {
            $book['name'] = $translations[1];
        } else {
            $book['name'] = $translations[0];
        }

        return $this->respond($book);
    }

    public function searchAction(Request $request): Response
    {
        $target = $request->get('target');

        if (!$target) {
            throw new NotFoundHttpException('Specify book name');
        }

        $books = $this->entityManager->getRepository(Book::class)->createQueryBuilder('b')
            ->where('LOWER(b.name) LIKE LOWER(:target)')
            ->setParameter('target', '%' . $target . '%')
            ->getQuery()
            ->getResult();


        if (empty($books)) {
            throw new NotFoundHttpException('Books are not found');
        }

        return $this->respond($books);
    }


}