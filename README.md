ПО: Git, Docker (v.3), Composer, Postman, terminal

1. Клонировать проект:

git clone git@gitlab.com:dev-area/books.git

2. В терминале перейти в папку проекта:

composer install

docker-compose up -d

3. Запустить миграции
docker-compose exec php-fpm php bin/console doctrine:migrations:migrate --no-interaction

4. В Postman, создать автора (/author/create):

http://localhost:8080/api/v1/author (POST)
Вкладка: Headers
установить key: Content-Type
установить value: application/json

Вкладка: Body
raw/JSON

{
"name": "Л. Толстой"
}

5. В Postman, создать книгу с автором (/book/create):

http://localhost:8080/api/v1/book   (POST)
Вкладка: Headers
установить key: Content-Type
установить value: application/json

Вкладка: Body
raw/JSON

{
"name": "War and Peace|Война и мир",
"author": [
    1
]
}

6. В Postman найти книгу (например: Мир)

http://localhost:8080/api/v1/book/search/Мир

7. В Postman мультиязычный метод (/{lang}/book/{Id})

http://localhost:8080/api/v1/en/book/1
или
http://localhost:8080/api/v1/ru/book/1

8. Тесты в терманале запустить:

phpunit


P.S. 
- Сделал простой тесты для Book сущности
- Миграции Version20201212170616 закоммитил, тк выбрал не самый лучший способ 
засеивать таблицы (формирую певичный ключ в запросе), доктрин хранит свой ключ (ошибка дублирования)
